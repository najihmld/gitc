<?php
/**
 * Konfigurasi dasar WordPress.
 *
 * Berkas ini berisi konfigurasi-konfigurasi berikut: Pengaturan MySQL, Awalan Tabel,
 * Kunci Rahasia, Bahasa WordPress, dan ABSPATH. Anda dapat menemukan informasi lebih
 * lanjut dengan mengunjungi Halaman Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Menyunting wp-config.php}. Anda dapat memperoleh pengaturan MySQL dari web host Anda.
 *
 * Berkas ini digunakan oleh skrip penciptaan wp-config.php selama proses instalasi.
 * Anda tidak perlu menggunakan situs web, Anda dapat langsung menyalin berkas ini ke
 * "wp-config.php" dan mengisi nilai-nilainya.
 *
 * @package WordPress
 */

// ** Pengaturan MySQL - Anda dapat memperoleh informasi ini dari web host Anda ** //
/** Nama basis data untuk WordPress */
define( 'DB_NAME', 'globalitc' );

/** Nama pengguna basis data MySQL */
define( 'DB_USER', 'root' );

/** Kata sandi basis data MySQL */
define( 'DB_PASSWORD', '' );

/** Nama host MySQL */
define( 'DB_HOST', 'localhost' );

/** Set Karakter Basis Data yang digunakan untuk menciptakan tabel basis data. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Jenis Collate Basis Data. Jangan ubah ini jika ragu. */
define('DB_COLLATE', '');

/**#@+
 * Kunci Otentifikasi Unik dan Garam.
 *
 * Ubah baris berikut menjadi frase unik!
 * Anda dapat menciptakan frase-frase ini menggunakan {@link https://api.wordpress.org/secret-key/1.1/salt/ Layanan kunci-rahasia WordPress.org}
 * Anda dapat mengubah baris-baris berikut kapanpun untuk mencabut validasi seluruh cookies. Hal ini akan memaksa seluruh pengguna untuk masuk log ulang.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g]yIWtH%0,bIB%?(wbZNi^m:TS7v[2x}2|ekrp49vbd@||1+3[a5#[q`FScM6sp=' );
define( 'SECURE_AUTH_KEY',  '*Au3*Zk,A];0Jz>b/=nGp@CgfbGy;9K  _da5v<Oy&{#(c|e#5bV0qodYMB<x$am' );
define( 'LOGGED_IN_KEY',    ']mPf9V*L@[UE7D]JE>5~-u1o:mNsXYA}{>E8+h[;LH(yx8p;zN<{N< 2-SrOq8)w' );
define( 'NONCE_KEY',        '5x9&/M%[HK![ Vn{1/?rXg[y@fOTL=,*k?0,pA8[rhRbM<&p$2,;@_qhoE,d:6B[' );
define( 'AUTH_SALT',        'B^v~q:)MzRf%oEkS6BOYK|*vUsU2bEiXI5C)Vtrz9DegEA.`(OE{+]{A)A@^*SW[' );
define( 'SECURE_AUTH_SALT', 'iWBau3cu6~j9N|/8tpc~3ryj|%-<.S2?4rtG?U2SsRN1tK.mXh7wf-)$:`u4zxqq' );
define( 'LOGGED_IN_SALT',   '?bNkmeBol6oEH8b0F#RV>NwDB6-i88DnBb{5S,DqBtSbOu7Yk,vq%p<LY<UB).mf' );
define( 'NONCE_SALT',       'yfv#q akY(R6iX[.4Xt3t|A_{.v9t$D|1jml@S+QKhz[le+HP4AY]b0dzL<f3:a?' );

/**#@-*/

/**
 * Awalan Tabel Basis Data WordPress.
 *
 * Anda dapat memiliki beberapa instalasi di dalam satu basis data jika Anda memberikan awalan unik
 * kepada masing-masing tabel. Harap hanya masukkan angka, huruf, dan garis bawah!
 */
$table_prefix = 'wp_';

/**
 * Untuk pengembang: Moda pengawakutuan WordPress.
 *
 * Ubah ini menjadi "true" untuk mengaktifkan tampilan peringatan selama pengembangan.
 * Sangat disarankan agar pengembang plugin dan tema menggunakan WP_DEBUG
 * di lingkungan pengembangan mereka.
 */
define('WP_DEBUG', false);

/* Cukup, berhenti menyunting! Selamat ngeblog. */

/** Lokasi absolut direktori WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Menentukan variabel-variabel WordPress berkas-berkas yang disertakan. */
require_once(ABSPATH . 'wp-settings.php');
